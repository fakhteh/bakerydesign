<?php
//****************************************************************
//****************************************************************
//************************                  **********************
//*************     		  orders Class      ******************
//************************                  **********************
//****************************************************************
//****************************************************************

class orders
{
private  $ID = 0;
private  $order_time = "";	

private  $db;
private  $Table_Name = "orders";
private  $Query_Result;
//-----------------------------------------------------------------------------------------------
public function __construct($db)
{
	$this->db=$db;
}
//-----------------------------------------------------------------------------------------------
public function Create_T_Orders()
{
	$sql ="
		  CREATE TABLE orders
		  (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
		  order_time  TIMESTAMP);";

	   $ret = $this->db->exec($sql);
	   if(!$ret){
		  echo $this->db->lastErrorMsg();
	   } else {
		  echo "Table created successfully\n";
	   }
}
//-----------------------------------------------------------------------------------------------
public function Add_Item($timestamp)
{
	$sql ="
		  INSERT INTO orders (order_time)
		  VALUES ( '$timestamp' );";

	   $ret = $this->db->exec($sql);
	   if(!$ret) {
		  return false;
	   } else {
		  return true;
	   }
}
//-----------------------------------------------------------------------------------------------
public function Select_All()
{
	$my_arr = array();$i=0;
	$sql = "SELECT * from orders;";

    $ret = $this->db->query($sql);
	while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
		$my_arr[$i]['ID'] = $row['ID'];
		$my_arr[$i]['Time'] = $row['order_time'];
		$i++;
      
   }
   return $my_arr;
}	
//-----------------------------------------------------------------------------------------------	
}
?>