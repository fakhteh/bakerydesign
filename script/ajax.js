
//******************************************************************
//******************************************************************
//  Ajax Connection
//******************************************************************
//******************************************************************

function MakeConnection()
{
	"use strict"; var httpRequest;
	if (window.XMLHttpRequest) // Mozilla, Safari, Opera
    {
    	httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType)
        {
        	httpRequest.overrideMimeType('text/html');
        }
	}
    else if (window.ActiveXObject)  // IE
    {
    	try	{	httpRequest = new ActiveXObject("Msxml2.XMLHTTP");   }
        catch (e)
        {
        	try	{     httpRequest = new ActiveXObject("Microsoft.XMLHTTP");    }
			catch (e) {}
		}
	}
    if (!httpRequest)
	{
      	alert('ERROR : Cannot create an XMLHTTP instance!');
	  	return false;
    }
	return httpRequest;
} 
//******************************************************************
//******************************************************************
// Set Content no java
//******************************************************************
//******************************************************************

function Set_Content(Proccess_Type,Page_Address,Page_Address_Query,SynType,Content_Id)
{ 
	
	"use strict"; var HttpConn = MakeConnection();
	if (Proccess_Type=='get')	HttpConn.open(Proccess_Type, Page_Address+'?'+Page_Address_Query,SynType);
	else	HttpConn.open(Proccess_Type, Page_Address,SynType);
	
	
	HttpConn.onreadystatechange = function ()
	{
			
		if ((HttpConn.readyState == 4)&&(HttpConn.status == 200)) 
		{
			var MyResponse = HttpConn.responseText.split('%$$%');
			if(Content_Id!==null){document.getElementById(Content_Id).innerHTML=MyResponse[0];}

			var Page_Address2 = Page_Address;

			if(Page_Address.indexOf('?')>0){Page_Address2 = Page_Address.substr(0,Page_Address.indexOf('?'));}
			
			var start = 1;

			for(var i=start ; i<MyResponse.length ; i++)	
			{	
				eval(MyResponse[i]);
			}
		}
	};
	if (Proccess_Type=='get')		HttpConn.send(null);
	else
	{
		HttpConn.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		HttpConn.send(Page_Address_Query);
	}
}

//******************************************************************
//******************************************************************
