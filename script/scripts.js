document.addEventListener('DOMContentLoaded', function(event) {
   document.getElementById('sun').addEventListener('click', function() {
	   night_mode();
   });
	
   document.getElementById('overlay').addEventListener('click', function() {
	   day_mode();
   });
	
   document.getElementById('mailbox_div').addEventListener('click', function() {
	   mailbox_falling();
   });
	
   document.getElementById('door').addEventListener('click', function() {
	   door_knock();
   });	
})

function night_mode(){
	let sun_moon_orbit = document.getElementById('sun_moon_orbit');
	let overlay = document.getElementById('overlay');
	
	sun_moon_orbit.style.transform = "rotate(40deg)";
	sun_moon_orbit.style.transitionDuration = "2s";
	
	overlay.classList.add("show");
}

function day_mode(){
	let sun_moon_orbit = document.getElementById('sun_moon_orbit');
	let overlay = document.getElementById('overlay');
	
	sun_moon_orbit.style.transform = "rotate(0deg)";
	
	overlay.classList.remove("show");
}

function mailbox_falling(){
	let mailbox_div = document.getElementById('mailbox_div');
	mailbox_div.classList.add('mailbox_falling');
}

/***************************************************************/
/** going to process page after clicking on the door ***********/
/***************************************************************/
function door_knock(){
	Set_Content('post','process.php','mode=new_order',true,'content_id');
}

function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("demo").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "ajax_info.txt", true);
  xhttp.send();
}