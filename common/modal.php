<!-- The Modal -->
<!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 id="modal-title" class="modal-title my-modal-title" style="float: left"></h4>
          <button type="button" class="close" data-dismiss="modal" style="float: right">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div id="modal-body" class="modal-body my-modal-body">
          Modal body..
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  